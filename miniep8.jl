using Test
function transformaCarta(x,y)
	if x[1]=='J'
		num=10
	else
		if x[1]=='Q'
			num=11
		else
			if x[1]=='K'
				num=12
			else
				if x[1]=='A'
					num=13
				else
					if(x[1]=='1')
						num=9
						x=x[1]*x[3]
					else
						num=parse(Int64,x[1])-1
					end
				end
			end
		end
	end
	if(y==1)
		if x[2]=='♠'
			num+=13
		else
			if x[2]=='♥'
			num+=26
			else
				if x[2]=='♣'
					num+=39
				end
			end
		end
	end
	return num
end
function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end
function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1])
				troca(v, j, j - 1)
			else
				break
			end
		j = j - 1
		end
	end
	return v
end
function compareByValue(x,y)
	numx=transformaCarta(x,0)
	numy=transformaCarta(y,0)
	if numx<numy
		return true
	end
	return false
end
function compareByValueAndSuit(x, y)
	numx=transformaCarta(x,1)
	numy=transformaCarta(y,1)
	if numx<numy
		return true
	end
	return false
end
function tests()
	@test compareByValue("2♠", "A♠")
	@test !compareByValue("K♥", "10♥")
	@test !compareByValue("10♠", "10♥")
	@test compareByValueAndSuit("2♠", "A♠")
	@test !compareByValueAndSuit("K♥", "10♥")
	@test compareByValueAndSuit("10♠", "10♥")
	@test compareByValueAndSuit("A♠", "2♥")
end